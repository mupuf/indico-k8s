# Indico Helm Charts

This repo is meant to provide all the necessary configuration to generate
an [Indico](https://getindico.io/) container, and run it in Kubernetes as an
Helm Charts.

**DISCLAIMER**: I am not a k8s expert, so don't trust my work! The work was
based on [indico-containers](https://github.com/indico/indico-containers).If you
find something wrong with this, please file an issue, or send me a merge request!

## Quick start

Assuming you have kubernetes set up (or minikube, if you run locally), you can
run indico like this:

    $ helmfile apply

and stopped like this:

    $ helmfile delete

## Updating to a new version of indico

Just run the following script:

    $ ./build_latest.sh
