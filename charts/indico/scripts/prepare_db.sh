#!/bin/bash

/opt/indico/set_user.sh
. /opt/indico/.venv/bin/activate

while ! pg_isready; do
    sleep 1
done

echo "CREATE USER $PG_USER_LOGIN WITH PASSWORD '$PG_USER_PASSWORD';" | psql
echo "CREATE DATABASE $PG_USER_DB OWNER $PG_USER_LOGIN;" | psql
echo 'CREATE EXTENSION unaccent; CREATE EXTENSION pg_trgm;' | psql -d $PG_USER_DB

psql -c 'SELECT COUNT(*) FROM events.events' -d $PG_USER_DB
if [ $? -eq 1 ]; then
    echo 'Preparing DB...'
    indico db prepare
fi
