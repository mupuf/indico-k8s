#!/bin/bash

/opt/indico/set_user.sh
. /opt/indico/.venv/bin/activate

echo 'Starting Indico...'
uwsgi /etc/indico/uwsgi.ini
