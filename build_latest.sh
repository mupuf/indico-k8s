#!/bin/sh

set +eux

LATEST_VERSION=$(curl --insecure https://api.github.com/repos/indico/indico/releases/latest | jq '.name[1:]' --raw-output)
echo Building Indico ${LATEST_VERSION}...
docker build  -t registry.freedesktop.org/mupuf/indico-k8s/indico:latest --build-arg tag=${LATEST_VERSION} containers/indico/
docker push registry.freedesktop.org/mupuf/indico-k8s/indico:latest
